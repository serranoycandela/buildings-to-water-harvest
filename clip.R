library(sf)
library(readr)
library(dplyr)

X85d_buildings <- read_csv("GitLab/buildings-to-water-harvest/data/85d_buildings.csv")
X85d_buildings <- X85d_buildings %>% select('confidence','area_in_meters', 'longitude', 'latitude')
X85d_buildings <- X85d_buildings %>% dplyr::filter(confidence >= 0.80)
points = st_as_sf(X85d_buildings, coords = c("longitude", "latitude"), crs = 4326)
#buildings = st_as_sf(X85d_buildings,crs=4326, wkt = "geometry")
X85d_buildings <- 0
munis = st_read("/home/fidel/GitLab/buildings-to-water-harvest/data/municipios_clipped_sa.shp")

points <- st_join(points, munis)

las_sumas <- points %>% 
  group_by(id) %>% 
  summarise(area_buildings = sum(area_in_meters))

las_sumas <- st_drop_geometry(las_sumas)

left_merged <- merge(munis, las_sumas, by = "id", all.x = TRUE)
st_write(left_merged, "GitLab/buildings-to-water-harvest/sumas.shp", layer_options = "ENCODING=UTF-8", delete_layer = TRUE)
