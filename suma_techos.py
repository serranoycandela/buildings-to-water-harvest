
params = {'INPUT':'/home/fidel/GitLab/buildings-to-water-harvest/data/municipios_clipped_sa.shp',
          'PREDICATE':[0,1],
          'JOIN':'delimitedtext://file:///home/fidel/GitLab/buildings-to-water-harvest/data/85d_buildings.csv?type=csv&maxFields=10000&detectTypes=yes&xField=longitude&yField=latitude&crs=EPSG:4326&spatialIndex=no&subsetIndex=no&watchFile=no',
          'JOIN_FIELDS':['area_in_meters'],
          'SUMMARIES':[5],
          'DISCARD_NONMATCHING':False,
          'OUTPUT':'/home/fidel/GitLab/buildings-to-water-harvest/data/municipios_sum_area_buildings.shp'}


processing.run("native:joinbylocationsummary", params)