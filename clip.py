import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt


from shapely.geometry import Point

FILE_NAME = "data/85d_buildings.csv"
#FILE_HEADER =
USE_COLS = ['confidence','area_in_meters', 'longitude', 'latitude']

df = pd.read_csv(
    FILE_NAME, delimiter=",", usecols=USE_COLS)

print(df.head())

puntos = gpd.GeoDataFrame(df,
    geometry = gpd.points_from_xy(df['longitude'], df['latitude']),
    crs = 'EPSG:4326')

print(puntos.head())

muni = gpd.read_file("data/municipios_clipped_sa.shp")

print(muni.head())



dfsjoin = gpd.sjoin(puntos, muni)
print(dfsjoin.head())

suma_por_id = dfsjoin.groupby(['id'])['area_in_meters'].sum()

print(suma_por_id.head())


# dfpolynew = muni.merge(dfpivot, how='left', on='id')
# print(dfpolynew.head())






# dfpivot = pd.pivot_table(dfsjoin,index='PolyID',columns='Food',aggfunc={'Food':len})
# dfpivot.columns = dfpivot.columns.droplevel()

# dfpolynew = polys.merge(dfpivot, how='left', on='PolyID')
